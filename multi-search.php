<?php
/*
Plugin Name: Multi Search
Plugin URI: 
Description: Multiple category and/or tag search
Author: Brad Hays
Version: 0.0.1
Author URI: http://www.nextstepsolutions.net
*/

require_once("multi-search-widget.php");

class multi_search{

	private $location=null;
	private $name="multi-search";
	private $icon='images/Icon.png';
	private $options;
	private $option_group='multi_search_group';
	private $categories=array();
	private $exclutions=null;
	private $definedVals=array();
	private $catListMax=5;

	function multi_search()
	{
		$this->__construct();
	} // function

	function __construct()
	{
		$this->location=dirname(__FILE__)."/";

		if(is_admin()){
			#require_once("admin/admin.php");
			#hooks
			register_activation_hook(__FILE__,array( &$this, 'plugin_install'));
			register_deactivation_hook(__FILE__,array( &$this, 'plugin_uninstall'));

			//add_action('wp_ajax_fusion_admin', array( &$this, 'admin_callback')); //json
			//add_action('wp_ajax_fusion_admin_update',array( &$this, 'admin_update')); //ajax

			add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
			add_action( 'admin_init', array( $this, 'page_init' ) );

			//Admin Page	
			//add_action('admin_menu',array( &$this, 'admin_menu'));
			
		} else{
			add_shortcode( 'multi_search', array( &$this, 'my_shortcode_handler') );
			add_filter( 'excerpt_more', array( &$this, 'new_excerpt_more' ) );

		}
		add_action('widgets_init', array( &$this, 'register_multi_search_widget') );
	}


	function register_multi_search_widget() {
	    register_widget('multi_search_widget');
	}

	function new_excerpt_more( $more ) {
		return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More</a>';
	}


	function my_shortcode_handler( $atts, $content=null, $code="" ) { 
		global $wpdb; 
	   // $atts    ::= array of attributes
	   // $content ::= text within enclosing form of shortcode element
	   // $code    ::= the shortcode found, when == callback name
	   //
	   // defaults: [multi_search op='default']

		extract( shortcode_atts( array(
			  'op'=>'default',
		      ), $atts ) );
		$op=strtolower($atts['op']); //just to be sure

		switch($op){
			
			default:
			//CSS
				wp_enqueue_style('multi_search.css', WP_PLUGIN_URL . '/'.$this->name. '/multi_search.css',false,'1.1','all');
			//JS
				wp_enqueue_script('jquery');
				$this->render_search_shortcode();					
			break;
			
							
		}
	}

	function plugin_install(){
		//add settings
	}

	function plugin_uninstall(){
		//remove settings
	}

	function admin_update(){
		//update logic for admin section
	}

	function admin_main(){

		require_once("admin/main.php");

	} // function

	

	function admin_menu(){

		//Main Menu

		$page_title=$this->name;
		$menu_title=$this->name;
		$capability = 'manage_options';
		$main_menu_slug=str_replace(" ","-",$this->name).'_menu';
		$function=array( &$this, 'admin_main');
		$icon_url=get_option('siteurl').'/wp-content/plugins/'.$this->name."/".$this->icon;
		$position=66;
		add_menu_page( $page_title, $menu_title, $capability, $main_menu_slug, $function, $icon_url, $position );
		// Create Sub Menus
			//add_submenu_page( $main_menu_slug, "Sign-in","Sign-in", $capability, "Sign-in"."_menu", array( &$this, 'signin') );
	}// function

	public function show_search(){
		require_once($this->location."forms/search.phtml");
	}

	private function avaliable_categories($parent=0){
		//filter categories by slug
		if(is_null($this->exclutions)){
			$this->exclutions=$this->get_exclutions();
		}
			$newCats=array();
			//echo "<br>Parent: $parent";
			$args = array(
				'type'                     => 'post',
				//'child_of'                 => 0,
				'parent'                   => $parent,
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 1,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false 

			); 

			foreach (get_categories($args) as $cat) {
				//echo "<br>is ".$cat->slug." in array"; 
				// echo '<pre>'.print_r($exclustions,true).'</pre>';
				if(!in_array($cat->slug, $this->exclutions)){
					//$newCats[]=$cat;
					if($cat->parent==0){
						$this->categories[$cat->cat_ID]=$cat;
						$this->categories[$cat->cat_ID]->children=$this->avaliable_categories($cat->cat_ID);
					}else{
						$cat->children=$this->avaliable_categories($cat->cat_ID);
						$newCats[]=$cat;
					}
				}else{
					// echo "<b>Excluded</b>";
				}
			}

			//flush();
			return $newCats;
	}

	private function get_exclutions(){
		if(is_null($this->options)){
			$this->options = get_option( 'multi_search_option' );
		}
		$exclusions=str_replace(" ","",esc_attr( $this->options['exclusions']));
		return explode(",",$exclusions);
	}

	private function get_searchpage(){
		if(is_null($this->options)){
			$this->options = get_option( 'multi_search_option' );
		}
		return get_permalink($this->options['searchpage']);
	}

	public function get_cat_list_max(){
		if(is_null($this->options)){
			$this->options = get_option( 'multi_search_option' );
		}
		return intval($this->options['maxin']);
	}

	/**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            ucwords($this->name), 
            'manage_options', 
            $this->name.'-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'multi_search_option' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2><?php echo ucwords($this->name); ?> Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( $this->option_group );   
                do_settings_sections( $this->name.'-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            $this->option_group, // Option group
            'multi_search_option', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Global Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            $this->name.'-admin' // Page
        );       

        add_settings_field(
            'exclusions', 
            'Exclusions (hide by comma spaced slugs)', 
            array( $this, 'exclusions_callback' ), 
            $this->name.'-admin', 
            'setting_section_id'
        );    

        add_settings_field(
            'searchpage', 
            'SearchPage', 
            array( $this, 'searchpage_callback' ), 
            $this->name.'-admin', 
            'setting_section_id'
        );  

        add_settings_field(
            'maxin', 
            'Max Items to display in "In"', 
            array( $this, 'maxin_callback' ), 
            $this->name.'-admin', 
            'setting_section_id'
        );     
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['exclusions'] ) )
            $new_input['exclusions'] = sanitize_text_field( $input['exclusions'] );

        $new_input['searchpage']=$input['searchpage'];
        $new_input['maxin']=intval($input['maxin']);
        
        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Global settings:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function exclusions_callback()
    {
        printf(
            '<input type="text" id="exclusions" name="multi_search_option[exclusions]" value="%s" />',
            isset( $this->options['exclusions'] ) ? esc_attr( $this->options['exclusions']) : ''
        );
    }
    public function maxin_callback()
    {
        printf(
            '<input type="text" id="maxin" name="multi_search_option[maxin]" value="%s" />',
            isset( $this->options['maxin'] ) ? esc_attr( $this->options['maxin']) : 5
        );
    }

    public function searchpage_callback()
    {
    	$args = array(
		    'depth'            => 0,
		    'child_of'         => 0,
		    'selected'         => isset( $this->options['searchpage'] ) ? esc_attr( $this->options['searchpage']) : 0,
		    'echo'             => 0,
		    'name'             => 'multi_search_option[searchpage]');

        $pages=wp_dropdown_pages($args);
        echo print_r($pages,true);
        // printf(
        //     '<input type="text" id="searchpage" name="multi_search_option[searchpage]" value="%s" />',
        //     isset( $this->options['searchpage'] ) ? esc_attr( $this->options['searchpage']) : ''
        // );
    }

    public function render_search_options($echo=false){
    	$output="";

    	if(empty($this->categories)){$this->avaliable_categories();}
    	$output="";
    	echo "<ul class='multi_search_options'>";
    	foreach ($this->categories as $category) {
    		$this->render_category($category, 0);
    	}
    	echo "</ul>";
    }

    public function render_search_shortcode(){
    	$this->render_search();
    	$this->render_search_results();
    }

    public function render_search(){
    	?>
    	<script>
			jQuery( document ).ready(function() {
			  jQuery(".sub_category_search").hide();
			});

			function getCategories(){
				var cats=jQuery(".search_checkbox:checked");
				var catArr=[];
				cats.each(function() {
				  	catArr.push( this.value );
				});
				var catArrStr = catArr.join(",");
				jQuery("#category_ids").val(catArrStr);
				jQuery("#multi-search-form").submit();
				return false;
			}	

		</script>
		<a href='#' onclick="jQuery('.search_checkbox').prop('checked', true);return false;">Select all</a> | <a href='#' onclick="jQuery('.search_checkbox').prop('checked', false);return false;">de-select all</a>
		<form id="multi-search-form" action="<?php echo $this->get_searchpage(); ?>" method="get">
				<input type="text" name="q" id="q" autocomplete="off" value="">
			<br>Search by category:<br>	
			<?php 
			echo $this->render_search_options(true);
			?>
			<button onclick="getCategories();return false;">Search</button>
			<!-- <input type="submit" value="Submit" onclick="getCategories();"> -->
		</form>

		<?php 
    }

    private function render_search_results(){
    	if(isset($_GET['multi-search'])){
			echo "<hr>";
			$s=$_GET['q'];
			$c=implode(",",$_GET['multi-search']);
				$search = new WP_Query("s=$s & cat=$c & showposts=-1");
				echo "Found ".$search->found_posts." results:";
				   if($search->have_posts()){
				      $this->get_cat_list_max(); //initialize 
				      while($search->have_posts()){
				         $search->the_post(); 
				         $categories=get_the_category();
				         $catLinks=array();
				         $i=0;
				         foreach ($categories as $cat) {
				         	if($this->catListMax<=$i){break;}
				         	$catLinks[]="<a href='".get_category_link($cat->cat_ID)."'>".$cat->name."</a>";
				         	$i++;
				         }
				         $link='href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"';
				         ?>
				        <div class='multi-search-container'>
					         <h2 class='post-title multi-search-result-title'><a <?php echo $link; ?>><?php the_title() ?></a></h2>
					         <div class='post-meta multi-search-meta'><span class='multi-search-catlist'>In:<?php echo implode(" / ", $catLinks); ?></span></div>
					         <div class='post-thumb multi-search-result-thumb'><a <?php echo $link; ?>><?php the_post_thumbnail(); ?></a></div>
					         <div class='post-content multi-search-result-content'><?php the_excerpt(); ?></div>      
					         <div class='more-link multi-search-result-more'><a <?php echo $link; ?>>Continue reading...</a></div>
						</div>			    
				<?php $i++; }
				   }else{
				      echo "<br>Oops, there are no posts.";
				   }
			
				//echo '<pre>'.print_r($search,true).'</pre>';
			echo "<br>End Results<hr>";
		}
    }

    private function get_defined_vals(){
    	if(empty($this->definedVals)){
    		if(isset($_GET['multi-search']) && is_array($_GET['multi-search'])){
    			$this->definedVals=array_flip($_GET['multi-search']);
    		}
    	}
    	return $this->definedVals;
    }

    private function render_category(&$category, $level){
    	$definedVals=$this->get_defined_vals();

    	$name=$category->name;
    	$hasChildren=sizeof($category->children)>0;
    	echo "<li>";
    	if($hasChildren){
    		echo "<a href='#' onclick=\"jQuery('#".$category->slug."_container').toggle(); return false;\">+</a> ";
    	}else{
    		echo "- ";
    	}
    	
    	echo $name." <input type='checkbox' id ='".$category->slug."' name ='multi-search[]' class='search_checkbox' value='".$category->cat_ID."' ".((isset($definedVals[$category->cat_ID]))?" CHECKED='CHECEKED'":"").">";
		if($hasChildren){
			echo "<div name='".$category->slug."_container' id='".$category->slug."_container' class='sub_category_search' style='display: block;'><ul>";
		}
    	
    	foreach ($category->children as $child) {
    		$this->render_category($child, $level+1);
    	}
    	if(sizeof($category->children)>0){
    		echo "</ul></div>";
    		}
    	echo "</li>";
    	
    }
};

new multi_search;